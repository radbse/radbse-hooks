import React, { createContext, useContext, useState } from 'react'

export const initialAlertState = { content: null, timeout: 0 }

export const AlertContext = createContext()

export const AlertProvider = ({ initialState, children }) => (
    <AlertContext.Provider value={useState(initialState)}>{children}</AlertContext.Provider>
)

/** returns an array with alert value and function to update it
 *
 * @returns {[]} [alert, setAlert]
 *
 */
export const useAlert = () => useContext(AlertContext)
