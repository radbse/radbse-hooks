import { useState, useRef, useEffect, useCallback, useReducer } from 'react'

export const useInput = initialValue => {
    const [value, setValue] = useState(initialValue)

    return [
        value,
        {
            value,
            onChange: useCallback(e => {
                setValue(e.target.value)
            }, []),
        },
        setValue,
    ]
}

export const useInputFocus = initialValue => {
    const [value, bind, set] = useInput(initialValue)
    const ref = useRef(null)

    useEffect(() => {
        ref.current.focus()
    }, [])

    return [value, { ...bind, ref }, set]
}

const defaultValues = {
    text: '',
    select: '',
    number: 0,
    boolean: false,
}
const actionTypes = {
    reset: 'reset',
    touched: 'touched',
    updateValue: 'updateValue',
    validationResult: 'validationResult',
}

const actions = {
    reset: (fieldName) => ({ type: actionTypes.reset, fieldName }),
    touched: fieldName => ({ type: actionTypes.touched, fieldName }),
    updateValue: (fieldName, value) => ({ type: actionTypes.updateValue, fieldName, payload: value }),
    validationResult: (fieldName, error, errormessage) => ({
        type: actionTypes.validationResult,
        fieldName,
        payload: { error, errormessage },
    }),
}

const FormField = (fieldState, dispatch, fieldArgs = {}) => {
    const requiredMessage = fieldArgs.requiredMessage || `${fieldState.initial.label || fieldArgs.name} is Required`

    const tryValidate = (value, touched) => {
        let result
        if (value && fieldArgs.validate) {
            result = fieldArgs.validate(value, fieldArgs.name)
        }

        if (!Boolean(result) && !fieldArgs.optional && touched && value === '') {
            result = requiredMessage
        }

        if (touched) {
            dispatch(actions.validationResult(fieldArgs.name, Boolean(result), result))
        }

        return !Boolean(result)
    }

    const setValue = v => {
        dispatch(actions.updateValue(fieldArgs.name, v))
        tryValidate(v, fieldState.current.touched)
    }

    const onChange = event => {
        const value = fieldArgs.valueFromChange ? fieldArgs.valueFromChange(event) : event.target.value
        const coercedValue = fieldArgs.normalize ? fieldArgs.normalize(value) : value
        setValue(coercedValue)
        tryValidate(coercedValue, true)
    }

    const onBlur = () => {
        dispatch(actions.touched(fieldArgs.name))
        tryValidate(fieldState.current.value, true)
    }

    const setValidationResult = result => {
        dispatch(actions.validationResult(fieldArgs.name, true, result))
    }

    const validate = () => {
        return tryValidate(fieldState.current.value, true)
    }

    return {
        props: {
            error: fieldState.current.error,
            errormessage: fieldState.current.errormessage,
            label: fieldState.initial.label,
            value: fieldState.current.value,
            // handlers
            onBlur,
            onChange,
        },
        meta: {
            touched: fieldState.current.touched,
            pristine: fieldState.current.pristine,
        },
        setValidationResult,
        setValue,
        validate,
    }
}

const BooleanField = (fieldState, dispatch, args = {}) => {
    const valueFromChange = event => event.target.checked

    const result = FormField(fieldState, dispatch, { ...args, valueFromChange })
    result.props.checked = result.props.value
    return result
}

const NumberField = (fieldState, dispatch, args = {}) => {
    return FormField(fieldState, dispatch, args)
}

const SelectField = (fieldState, dispatch, args = {}) => {
    const fieldProps = FormField(fieldState, dispatch, args)
    fieldProps.props.options = args.options
    return fieldProps
}

const TextField = (fieldState, dispatch, args = {}) => {
    return FormField(fieldState, dispatch, args)
}

const fieldReducer = (state, { type, fieldName = '', payload }) => {
    switch (type) {
        case actionTypes.updateValue: {
            const fieldState = { ...state.get(fieldName) }
            fieldState.current.value = payload
            fieldState.current.pristine = payload === fieldState.initial.value
            fieldState.current.touched = true
            state.set(fieldName, fieldState)
            return new Map(state.entries())
        }
        case actionTypes.touched: {
            const fieldState = { ...state.get(fieldName) }
            fieldState.current.touched = true
            state.set(fieldName, fieldState)
            return new Map(state.entries())
        }
        case actionTypes.validationResult: {
            const { error, errormessage } = payload
            const fieldState = { ...state.get(fieldName) }
            fieldState.current.error = error
            fieldState.current.errormessage = errormessage
            state.set(fieldName, fieldState)
            return new Map(state.entries())
        }
        case actionTypes.reset: {
            const fieldState = { ...state.get(fieldName) }
            fieldState.current.value = fieldState.initial.value
            state.set(fieldName, fieldState)
            return new Map(state.entries())
        }
        default:
            return state
    }
}

const generateDefaultFieldState = (field, initialValues, options) => {
    if (!field.name) throw new Error('name is required on field')

    const initialValue = initialValues[field.name]

    const value = initialValue ? initialValue : field.value ? field.value : defaultValues[field.type || 'text']

    return {
        initial: {
            type: field.type || 'text',
            value,
            optional: field.optional || false,
            label: field.label,
            field,
        },
        current: {
            errormessage: field.errormessage || '',
            error: false,
            pristine: true,
            touched: false,
            value,
        },
    }
}

const getFieldState = (field, initialValues, options) => {
    return generateDefaultFieldState(field, initialValues, options)
}

const getInitialState = (fields, initialValues = new Map(), options = {}) => {
    return fields.reduce((acc, field) => {
        return acc.set(field.name, getFieldState(field, initialValues, options))
    }, new Map())
}

const useFields = (fields, options = {}, initialValues = new Map()) => {
    const initialState = getInitialState(fields, initialValues, options)
    const [state, dispatch] = useReducer(fieldReducer, initialState)

    const fieldData = Array.from(state.keys()).reduce((acc, fieldName) => {
        const fieldState = state.get(fieldName)
        const fieldType = fieldState.initial.type
        const field = fieldState.initial.field

        switch (fieldType) {
            case 'select':
                acc[fieldName] = SelectField(fieldState, dispatch, field)
                break
            case 'boolean':
                acc[fieldName] = BooleanField(fieldState, dispatch, field)
                break
            case 'number':
                acc[fieldName] = NumberField(fieldState, dispatch, field)
                break
            case 'text':
            default:
                acc[fieldName] = TextField(fieldState, dispatch, field)
                break
        }
        return acc
    }, {})

    return [fieldData, state, dispatch]
}

const getFieldValues = fieldData => {
    return Object.entries(fieldData).reduce((acc, [k, v]) => {
        acc[k] = v.props.value
        return acc
    }, {})
}

const getFieldProps = fieldData => {
    return Object.entries(fieldData).reduce((acc, [key, v]) => {
        acc[key] = {
            ...v.props,
        }
        return acc
    }, {})
}

const mergeFormValues = (state, initialState) => {
    return Array.from(state.entries()).reduce((acc, [key, fieldData]) => {
        acc[key] = fieldData.current.value
        return acc
    }, initialState)
}

export const useForm = ({ fields, submit, validate, options = {}, initialValues = new Map() }) => {
    const [fieldData, state, dispatch] = useFields(fields, options, initialValues)
    const [errors, setErrors] = useState(null)
    const tryValidateForm = () => {
        if (validate) {
            const values = getFieldValues(fieldData)
            let results = {}
            let validators = validate
            if (!Array.isArray(validate)) {
                validators = [validate]
            }
            for (let validator of validators) {
                results = {
                    ...results,
                    ...validator(values),
                }
            }
            return results
        }

        return {}
    }

    const trySubmitForm = () => {
        let isFormValid = true
        setErrors(null)
        const formResults = tryValidateForm()

        Object.entries(fieldData).forEach(([key, field]) => {
            if (formResults[key]) {
                field.setValidationResult(formResults[key])
                isFormValid = false
            } else {
                // only need to run field validation if there is already a form level issue with it.
                const isFieldValid = field.validate()
                if (!isFieldValid)
                    formResults[key] = field.props.errormessage || `${field.props.label || key} is Required`
                isFormValid = isFormValid && isFieldValid
            }
        })

        if (!isFormValid) setErrors(formResults)

        if (isFormValid && submit) {
            submit(mergeFormValues(state, initialValues))
        }
    }

    const resetForm = () => {
        setErrors(null)
        Object.entries(fieldData).forEach(([key, field]) => {
            dispatch(actions.reset(key))
        })
    }

    const setValue = (fieldName, value) => {
        fieldData[fieldName].setValue(value)
    }

    const setValues = (form) => {
        Object.entries(form).forEach(([key, value]) => {
            if (fieldData.hasOwnProperty(key)) {
                fieldData[key].setValue(value)
            }
        })
    }

    return [
        { ...getFieldProps(fieldData) },
        {
            setValue,
            setValues,
            submit: trySubmitForm,
            validate: tryValidateForm,
            reset: resetForm,
            errors,
        },
    ]
}
