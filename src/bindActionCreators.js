function bindActionCreator(actionCreator, dispatch) {
    return function() {
        return dispatch(actionCreator.apply(this, arguments))
    }
}

export function bindActionCreators(actionCreators, dispatch) {
    if (typeof actionCreators === 'function') {
        return bindActionCreator(actionCreators, dispatch)
    }

    if (typeof actionCreators !== 'object' || actionCreators === null) {
        throw new Error(
            `bindActionCreators expected an object or a function, instead received ${
                actionCreators === null ? 'null' : typeof actionCreators
            }. ` + `Did you write "import ActionCreators from" instead of "import * as ActionCreators from"?`
        )
    }

    const boundCreators = {}
    for (const key in actionCreators) {
        const actionCreator = actionCreators[key]
        if (typeof actionCreator === 'function') {
            boundCreators[key] = bindActionCreator(actionCreator, dispatch)
        } else {
            boundCreators[key] = actionCreator
        }
    }
    return boundCreators
}
