import React, { useCallback, useState } from 'react'
import logo from './logo.svg'
import './App.css'

import { useInput, useInputFocus } from './hooks/radbse-hooks'

const MyForm = ({ onSignin }) => {
    const [username, bindUsername] = useInputFocus('')
    const [password, bindPassword] = useInput('')
    const handleSubmit = useCallback(
        e => {
            e.preventDefault()
            onSignin(username, password)
        },
        [onSignin, password, username]
    )
    return (
        <form style={{ display: 'flex', flexDirection: 'column' }}>
            <div>
                <label>username</label>
                <input {...bindUsername} />
            </div>
            <div>
                <label>password</label>
                <input type="password" {...bindPassword} />
            </div>
            <button onClick={handleSubmit}>submit</button>
        </form>
    )
}
function App() {
    const [login, setLogin] = useState({ username: '', password: '' })
    const handleSignin = useCallback(
        (username, password) => {
            setLogin({ ...login, username, password })
        },
        [login]
    )
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                    Edit <code>src/App.js</code> and save to reload.
                </p>
                <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
                    Learn React
                </a>
                <MyForm onSignin={handleSignin} />
                <div>
                    <p>signed in with</p>
                    <p>username: {login.username}</p>
                    <p>password: {login.password}</p>
                </div>
            </header>
        </div>
    )
}

export default App
