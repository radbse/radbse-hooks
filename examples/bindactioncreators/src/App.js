import React, { createContext, useReducer, useContext, useCallback } from 'react'
import logo from './logo.svg'
import './App.css'

import { bindActionCreators } from './hooks/radbse-hooks'

const initialValue = { count: 0 }
const CounterContext = createContext(initialValue)

const reducer = (state, action) => {
    switch (action.type) {
        case 'increment':
            return { count: state.count + 1 }
        case 'decrement':
            return { count: state.count - 1 }
        default:
            throw new Error()
    }
}

const actionCreators = {
    increase: () => ({ type: 'increment' }),
    decrease: () => ({ type: 'decrement' }),
}

const Counter = () => {
    const { actions, state } = useContext(CounterContext)
    const handleIncrement = useCallback(() => {
        actions.increase()
    }, [actions])
    const handleDecrement = useCallback(() => {
        actions.decrease()
    }, [actions])
    return (
        <div>
            <label>{state.count}</label>
            <button onClick={handleIncrement}>+</button>
            <button onClick={handleDecrement}>-</button>
        </div>
    )
}

function App() {
    const [state, dispatch] = useReducer(reducer, initialValue)

    return (
        <CounterContext.Provider value={{ state, actions: bindActionCreators(actionCreators, dispatch), dispatch }}>
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>
                        Edit <code>src/App.js</code> and save to reload.
                    </p>
                    <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
                        Learn React
                    </a>
                    <Counter />
                </header>
            </div>
        </CounterContext.Provider>
    )
}

export default App
