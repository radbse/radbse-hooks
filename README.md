This project is a collection of reusable hooks.

# Forms

## useInput

Store value in component state and easily bind to input.

```
import { useInput } from '@radbse/hooks';

const MyComponent = () => {
    const [firstName, firstNameBind, setFirstName] = useInput("");

    return (
        <div>
            <input type="text" {...firstNameBind}>
        </div>
    )
}
```

## useInputFocus

Store value in component state and easily bind to input.
On first render, the input will have focus.

```
import { useInputFocus } from '@radbse/hooks';

const MyComponent = () => {
    const [firstName, firstNameBind, setFirstName] = useInputFocus("");

    return (
        <div>
            <input type="text" {...firstNameBind}>
        </div>
    )
}
```

## useForm

_The following examples utilize Semantic-UI inputs_

#### Simple Form

```
const SimpleForm = () => {
  const [fields, form] = useForm({
    fields: [
      { name: 'fullName', label: 'Full Name' },
      { name: 'nickname', label: 'Nickname' },
    ],
    submit: (fields) => {
        //Do Something
    }
  })

  return (
    <Form onSubmit={form.submit}>
        <Form.Input {...fields.fullName} />
        <Form.Input {...fields.nickname} />
        <Button primary>Submit</Button>
    </Form>
  )
}

export { SimpleForm }
```

#### Optional Fields

```
const SimpleForm = () => {
  const [fields, form] = useForm({
    fields: [
      { name: 'fullName', label: 'Full Name' },
      { name: 'nickname', label: 'Nickname', optional: true },
    ],
    submit: (fields) => {
        //Do Something
    }
  })

  return (
    <Form onSubmit={form.submit}>
        <Form.Input {...fields.fullName} />
        <Form.Input {...fields.nickname} />
        <Button primary>Submit</Button>
    </Form>
  )
}

export { SimpleForm }
```

#### Field level validation

```
const SimpleForm = () => {
  const [fields, form] = useForm({
    fields: [
      { name: 'fullName', label: 'Full Name' },
      { name: 'nickname', label: 'Nickname', optional: true },
      { name: 'phone', label: 'Phone', validate: validatePhone },
    ],
    submit: (fields) => {
        //Do Something
    }
  })

  return (
    <Form error onSubmit={form.submit}>
        <Form.Input {...fields.fullName} />
        <Form.Input {...fields.nickname} />
        <Form.Input {...fields.phone} />
        {form.errors ? <Message error list={Object.values(form.errors)} /> : null}
        <Button primary>Submit</Button>
    </Form>
  )
}

export { SimpleForm }
```

#### Field input normalization

```
const SimpleForm = () => {
  const [fields, form] = useForm({
    fields: [
      { name: 'fullName', label: 'Full Name' },
      { name: 'nickname', label: 'Nickname', optional: true },
      { name: 'phone', label: 'Phone', validate: validatePhone, normalize: normalizePhone },
    ],
    submit: (fields) => {
        //Do Something
    }
  })

  return (
    <Form error onSubmit={form.submit}>
        <Form.Input {...fields.fullName} />
        <Form.Input {...fields.nickname} />
        <Form.Input {...fields.phone} />
        {form.errors && <Message error list={Object.values(form.errors)} />}
        <Button primary>Submit</Button>
    </Form>
  )
}

export { SimpleForm }
```
